// Activity No. 3
db.rooms.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic needs",
	rooms_available: 10,
	isAvailable: false
})
// Activity No. 3 END


// Activity No. 4
db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false	
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false	
	}
])
// Activity No. 4 END

// Activity No. 5
db.rooms.find({name:"double"})
// Activity No. 5 END

// Activity No. 6
db.rooms.updateOne(
	{
		name: "queen"	
	},
	{
		$set: {
			rooms_available: 0
		}
	}
)
// Activity No. 6 END

// Activity No. 7
db.rooms.deleteMany({
	rooms_available: 0
})
// Activity No. 7 END